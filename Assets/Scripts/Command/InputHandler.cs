using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public PlayerMovement playerMovement;
    public PlayerWeapon playerWeapon;

    Queue<BaseCommand> commands = new Queue<BaseCommand>();

    private void FixedUpdate()
    {
        BaseCommand moveCommand = InputMovementHandling();
        if(moveCommand != null)
        {
            commands.Enqueue(moveCommand);
            moveCommand.Execute();
        }
    }

    private void Update()
    {
        BaseCommand shootCommand = InputShootingHandling();
        if(shootCommand != null)
        {
            shootCommand.Execute();
        }

        BaseCommand switchCommand = InputSwitchHandling();
        if(switchCommand != null)
        {
            switchCommand.Execute();
        }

    }

    BaseCommand Undo()
    {
        if(commands.Count > 0)
        {
            BaseCommand undoCommand = commands.Dequeue();
            undoCommand.UnExecute();
        }
        return null;
    }

    BaseCommand InputMovementHandling()
    {
        //Move Right
        if (Input.GetKey(KeyCode.D))
        {
            return new MoveCommand(playerMovement, 1, 0);
        }
        //Move Left
        else if (Input.GetKey(KeyCode.A)){
            return new MoveCommand(playerMovement, -1, 0);
        }
        //Move Up
        else if (Input.GetKey(KeyCode.W))
        {
            return new MoveCommand(playerMovement, 0, 1);
        }
        //Move Down
        else if (Input.GetKey(KeyCode.S))
        {
            return new MoveCommand(playerMovement, 0, -1);
        }
        //Undo Movement
        else if (Input.GetKey(KeyCode.Z))
        {
            return Undo();
        }
        else
        {
            return new MoveCommand(playerMovement, 0, 0);
        }
    }

    BaseCommand InputShootingHandling()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            return new ShootCommand(playerWeapon);
        }
        else
        {
            return null;
        }
    }

    BaseCommand InputSwitchHandling()
    {   
        //Scroll up
        if(Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            return new SwitchWeaponCommand(playerWeapon, false, -1);
        }
        else if(Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            return new SwitchWeaponCommand(playerWeapon, true, -1);
        }
        //First Weapon
        else if (Input.GetKey(KeyCode.Alpha1))
        {
            return new SwitchWeaponCommand(playerWeapon, false, 1);
        }
        //Second Weapon
        else if (Input.GetKey(KeyCode.Alpha2))
        {
            return new SwitchWeaponCommand(playerWeapon, false, 2);
        }
        //Third Weapon
        else if (Input.GetKey(KeyCode.Alpha3))
        {
            return new SwitchWeaponCommand(playerWeapon, false, 3);
        }
        //Fourth Weapon
        else if (Input.GetKey(KeyCode.Alpha4))
        {
            return new SwitchWeaponCommand(playerWeapon, false, 4);
        }
        else
        {
            return null;
        }
    }
}
