
public class SwitchWeaponCommand : BaseCommand
{
    PlayerWeapon playerWeapon;
    bool isNext;
    int ind;


    public SwitchWeaponCommand(PlayerWeapon _playerWeapon, bool _isNext, int _ind)
    {
        playerWeapon = _playerWeapon;
        isNext = _isNext;
        ind = _ind;
    }

    private void ScrollWeapon()
    {
        if(ind != -1)
        {
            return;
        }

        if (isNext)
        {
            playerWeapon.NextWeapon();
        }
        else
        {
            playerWeapon.PreviousWeapon();
        }
    }

    private void SelectWeapon()
    {
        if (ind == -1)
        {
            return;
        }
        else
        {
            playerWeapon.SelectWeapon(ind);
        }
    }

    public override void Execute()
    {
        SelectWeapon();
        ScrollWeapon();
    }

    public override void UnExecute()
    {

    }
}
