
public class MoveCommand : BaseCommand
{
    PlayerMovement playerMovement;
    float horizontal, vertical;

    public MoveCommand(PlayerMovement _playerMovement, float _horizontal, float _vertical)
    {
        playerMovement = _playerMovement;
        horizontal = _horizontal;
        vertical = _vertical;
    }

    public override void Execute()
    {
        playerMovement.Moving(horizontal, vertical);
        playerMovement.Animating(horizontal, vertical);
    }

    public override void UnExecute()
    {
        playerMovement.Moving(-horizontal, -vertical);
        playerMovement.Animating(-horizontal, -vertical);
    }
}
