
public class ShootCommand : BaseCommand
{
    PlayerWeapon playerWeapon;

    public ShootCommand(PlayerWeapon _playerWeapon)
    {
        playerWeapon = _playerWeapon;
    }

    public override void Execute()
    {
        playerWeapon.Shoot();
    }

    public override void UnExecute()
    {

    }
}
