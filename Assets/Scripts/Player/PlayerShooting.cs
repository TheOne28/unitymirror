﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;                  
    public float timeBetweenBullets = 0.0f;        
    public float range = 100f;
   

    public GameObject lineRendererPrefab;

    float timer;                                    
    int shootableMask;                             
    float effectsDisplayTime = 0.2f;
    int shotgunBullet = 4;
    float scattering = 10.0f;

    Ray shootRay;                                   
    RaycastHit shootHit;                            
    ParticleSystem gunParticles;                    
    AudioSource gunAudio;                           
    
    List<GameObject> lineRenderers = new List<GameObject>();
                     
    void Awake()
    {
        shootableMask = LayerMask.GetMask("Shootable");
        gunParticles = GetComponent<ParticleSystem>();
        gunAudio = GetComponent<AudioSource>();

        for(int i = 0; i < shotgunBullet; i++)
        {
            lineRenderers.Add(Instantiate(lineRendererPrefab, transform));
            lineRenderers[i].gameObject.transform.SetParent(transform, false);
            lineRenderers[i].gameObject.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
        }

    }

    void Update()
    {
        timer += Time.deltaTime;


        if (timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects();
        }

    }

    public void DisableEffects()
    {
        Light light;
        LineRenderer lineRenderer;

        for(int i = 0; i < shotgunBullet; i++)
        {
            light = lineRenderers[i].GetComponent<Light>();
            lineRenderer = lineRenderers[i].GetComponent<LineRenderer>();

            light.enabled = false;
            lineRenderer.enabled = false;
        }

    }

    public void Shoot()
    {
        Light gunLight = lineRenderers[0].GetComponent<Light>();
        LineRenderer gunLine = lineRenderers[0].GetComponent<LineRenderer>();

        timer = 0f;

        gunAudio.Play();

        gunLight.enabled = true;

        gunParticles.Stop();
        gunParticles.Play();

        gunLine.enabled = true;
        gunLine.SetPosition(0, transform.position);

        shootRay.origin = transform.position;

        shootRay.direction = transform.forward;

        if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();

            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(damagePerShot, shootHit.point);
            }

            gunLine.SetPosition(1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition(1, shootRay.direction * range);
        }
    }

    public void ShotgunShoot()
    {
        timer = 0f;

        gunAudio.Play();

        gunParticles.Stop();
        gunParticles.Play();

        int angle = 90 / ((shotgunBullet / 2) + 1) ;

        for(int i = 0; i < shotgunBullet; i++)
        {
            Light gunLight = lineRenderers[i].GetComponent<Light>();
            LineRenderer gunLine = lineRenderers[i].GetComponent<LineRenderer>();

            gunLight.enabled = true;
            gunLine.enabled = true;

            gunLine.SetPosition(0, transform.position);
            shootRay.origin = transform.position;

/*            Vector3 randomDirection = transform.forward;
            randomDirection += Random.Range(-scattering, scattering) * transform.right;*/
            
            if(i < shotgunBullet / 2)
            {
                shootRay.direction = Quaternion.Euler(0, -angle * (i + 1), 0) * transform.forward;
            }
            else
            {
                shootRay.direction = Quaternion.Euler(0, angle * (i - (shotgunBullet / 2)), 0) * transform.forward;
            }

            if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
            {
                EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();

                if (enemyHealth != null)
                {
                    enemyHealth.TakeDamage(damagePerShot, shootHit.point);
                }

                gunLine.SetPosition(1, shootHit.point);
            }
            else
            {
                gunLine.SetPosition(1, shootRay.direction * range);
            }
        }
    }
}