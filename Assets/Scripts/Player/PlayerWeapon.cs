using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
    public enum Weapon
    {
        Gun,
        Shotgun,
        Sword,
        Bow,
    }

    [SerializeField]
    PlayerShooting playerShooting;

    Weapon currentWeapon;

    // Start is called before the first frame update
    void Awake()
    {
        currentWeapon = Weapon.Gun;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void NextWeapon()
    {
        switch (currentWeapon)
        {
            case Weapon.Gun:
                currentWeapon = Weapon.Shotgun;
                break;
            case Weapon.Shotgun:
                currentWeapon = Weapon.Sword;
                break;
            case Weapon.Sword:
                currentWeapon = Weapon.Bow;
                break;
            case Weapon.Bow:
                currentWeapon = Weapon.Gun;
                break;
        }
    }

    public void PreviousWeapon()
    {
        switch (currentWeapon)
        {
            case Weapon.Gun:
                currentWeapon = Weapon.Bow;
                break;
            case Weapon.Shotgun:
                currentWeapon = Weapon.Gun;
                break;
            case Weapon.Sword:
                currentWeapon = Weapon.Shotgun;
                break;
            case Weapon.Bow:
                currentWeapon = Weapon.Sword;
                break;
        }
    }

    public void SelectWeapon(int ind)
    {
        switch (ind)
        {
            case 1:
                currentWeapon = Weapon.Gun;
                break;
            case 2:
                currentWeapon = Weapon.Shotgun;
                break;
            case 3:
                currentWeapon = Weapon.Sword;
                break;
            case 4:
                currentWeapon = Weapon.Bow;
                break;
            default:
                throw new System.Exception("Weapon Not avaliable");
        }

    }

    public void Shoot()
    {
        switch (currentWeapon)
        {
            case Weapon.Gun:
                playerShooting.Shoot();
                break;
            case Weapon.Shotgun:
                playerShooting.ShotgunShoot();
                break;
        }
    }
}
